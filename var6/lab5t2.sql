drop view if exists view_book_title;
drop view if exists view_book_author;
drop view if exists view_book_publisher;
drop view if exists view_book_keyword;
drop view if exists view_book_keyword_part;
drop view if exists view_book_count;
drop view if exists view_books_due;

-- Поиск книг по названию
create view view_book_title as select * from book where title = 'Test Book';
-- Поиск книг по автору
create view view_book_author as
select * from book inner join authorship using(isbn)
inner join author using(author_id)
where author.name = 'Test'
and author.surname = 'Testovich';
-- Поиск книг по издательсвту
create view view_book_publisher as select * from book inner join publisher using(publisher_id) where publisher.name = 'Test Publisher';
-- Поиск книг по ключевому слову (через массивы)
create view view_book_keyword as select * from book where string_to_array(keywords, ' ') @> ARRAY['Test'];
-- Поиск книг по части ключевого слова (через like)
create view view_book_keyword_part as select * from book where keywords like '%Tes%';
-- Количество книг в каждом катологе
create view view_book_count as
select catalogue_id, count(isbn) from catalogue
inner join genre using(catalogue_id) inner join book using(genre_id)
-- Книги, подлежащие возврату в ближайшие 30 дней
group by catalogue_id;
create view view_books_due as
select title, date_due from loan inner join book using(isbn)
where date_due - current_date < 30;
