drop table if exists Груз;
drop table if exists Грузовик;
drop table if exists Пункт;
drop table if exists Отправитель;

create table Отправитель (
	Код_отправителя integer primary key,
	Наименование varchar (255),
	Расположение varchar (255)
);

create table Пункт (
	Код_пункта integer primary key,
	Код_отправителя integer references Отправитель (Код_отправителя),
	Наименование varchar (255)
);

create table Грузовик (
	Номер_грузовика integer primary key,
	Марка varchar (255),
	Грузоподъёмность integer,
	Водитель varchar (255)
);

create table Груз (
	Код_груза integer primary key,
	Код_отправителя integer references Отправитель (Код_отправителя),
	Номер_грузовика integer references Грузовик (Номер_грузовика),
	Наименование varchar (255),
	Единица_изм varchar (255),
	Количество integer not null,
	Вес numeric 
);

insert into Отправитель (Код_отправителя, Наименование, Расположение)
values (1, 'Socket.by', 'г. Минск');
insert into Пункт (Код_пункта, Код_отправителя, Наименование)
values (1, 1, 'Склад 1');
insert into Грузовик (Номер_грузовика, Марка, Грузоподъёмность, Водитель)
values (1, 'Volkswagen', 10, 'Петя');
insert into Груз 
(Код_груза, Код_отправителя, Номер_грузовика, Наименование, Единица_изм, Количество, Вес) 
values (1001, 1, 1, 'Телефон', 'ед.', 1, 0.5);
