# ЛР 3

# Задание 3.1
- Задание 3.1.1. Выдать информацию о местоположении отдела продаж (SALES) компании.

```
select * from dept where deptname = 'SALES';
```

- Задание 3.1.2. Выдать информацию об отделах, расположенных в Chicago и New York.

```
select * from dept where deptaddr = 'CHICAGO' or deptaddr = 'NEW YORK';
```

- Задание 3.1.3. Найти минимальную заработную плату, начисленную в 2009 году.

```
select min(salvalue) from salary where year = '2009';
```

- Задание 3.1.4. Выдать информацию обо всех работниках, родившихся не позднее 1 января 1960 года.

```
select * from emp where birthdate <= date '1960-01-01';
```

- Задание 3.1.5. Подсчитать число работников, сведения о которых имеются в базе данных .

```
select count(*) from emp;
```

- Задание 3.1.6. Найти работников, чьё имя состоит из одного слова. Имена выдать на нижнем регистре, с удалением стоящей справа буквы t.

```
select lower(regexp_replace(empname, '.T', 'T')) from emp where empname not like '% %';
```

- Задание 3.1.7. Выдать информацию о работниках, указав дату рождения в формате день(число), месяц(название), год(название). Тоже, но год числом.

```
select empname, to_char(birthdate, 'DD Month YYYY') from emp;
```

- Задание 3.1.8. Выдать информацию о должностях, изменив названия должности “CLERK” и “DRIVER” на “WORKER”.

```
select replace(replace(jobname, 'DRIVER', 'WORKER'), 'CLERK', 'WORKER') from job;
```

- Задание 3.1.9. Определите среднюю зарплату за годы, в которые были начисления не менее чем за три месяца. (Ипользовать HAVING)

```
select year, avg(salvalue) from salary group by year having count(distinct month) >= 3;
```

- Задание 3.1.10. Выведете ведомость получения зарплаты с указанием имен служащих.
	
```
select emp.empno, empname, month, year, salvalue from emp inner join salary on emp.empno = salary.empno;
```

- Задание 3.1.11. Укажите сведения о начислении сотрудникам зарплаты, попадающей в вилку: минимальный оклад по должности - минимальный оклад по должности плюс пятьсот. Укажите соответствующую вилке должность.

```
select empname, jobname, salvalue, minsalary
from emp inner join career on emp.empno = career.empno
inner join job on job.jobno = career.jobno
inner join salary on emp.empno = salary.empno
where salvalue > minsalary and salvalue < minsalary + 500;
```

- Задание 3.1.12. Укажите сведения о заработной плате, совпадающей с минимальными окладами по должностям (с указанием этих должностей). (Использовать "equi" join)

```
select * from salary join emp on salary.empno = emp.empno
join career on emp.empno = career.empno
join job on career.jobno = job.jobno
where salary.salvalue = job.minsalary;
```

- Задание 3.1.13. Найдите сведения о карьере сотрудников с указанием вместо номера сотрудника его имени. (NATURAL JOIN)

```
select * from emp natural join career where empname = 'ALLEN';
```

- Задание 3.1.14. Найдите сведения о карьере сотрудников с указанием вместо номера сотрудника его имени. (INNER JOIN)

```
select * from emp
inner join career on emp.empno = career.empno where empname = 'ALLEN';
```

- Задание 3.1.15. Выдайте сведения о карьере сотрудников с указанием их имён, наименования должности, и названия отдела.

```
select empname, deptname, jobname from emp
full outer join career using(empno)
full outer join job using(jobno)
full outer join dept using(deptno);
```

- Задание 3.1.16. Выдайте сведения о карьере сотрудников с указанием их имён. Выдайте сведения о карьере сотрудников с указанием их имён. (LEFT OUTER JOIN)

```
select * from emp right outer join career using(empno) right outer join job using(jobno) where empname = 'ALLEN';
```

- Задание 3.1.16. Выдайте сведения о карьере сотрудников с указанием их имён. Выдайте сведения о карьере сотрудников с указанием их имён. (RIGHT OUTER JOIN)

```
select * from emp left outer join career using(empno) left outer join job using(jobno) where empname = 'ALLEN';
```

- Задание 3.1.16. Выдайте сведения о карьере сотрудников с указанием их имён. Выдайте сведения о карьере сотрудников с указанием их имён. (FULL OUTER JOIN)

```
select * from emp full outer join career using(empno) full outer join job using(jobno) where empname = 'ALLEN';
```

# Задание 3.2

- ПОДЗАПРОСЫ, ВЫБИРАЮЩИЕ ОДНУ СТРОКУ

	- Задание 3.2.1. Найти имена сотрудников, получивших за годы начисления зарплаты минимальную зарплату.

```
select * from emp
inner join salary using(empno) where salvalue = (select min(salvalue)
from salary);
```

- ПОДЗАПРОСЫ, ВОЗВРАЩАЮЩИЕ БОЛЕЕ ОДНОЙ СТРОКИ

	- Задание 3.2.2. Найти имена сотрудников, работавших или работающих в тех же отделах, в которых работал или работает сотрудник с именем RICHARD MARTIN.

```
select empname from emp
inner join career using(empno) where deptno in
(select deptno from emp
inner join career using(empno)
inner join dept using(deptno)
where empname = 'RICHARD MARTIN') group by empname;
```

- СРАВНЕНИЕ БОЛЕЕ ЧЕМ ПО ОДНОМУ ЗНАЧЕНИЮ

	- Задание 3.2.3. Найти имена сотрудников, работавших или работающих в тех же отделах и должностях, что и сотрудник 'RICHARD MARTIN'.

```
select empname from career
inner join emp using(empno) where jobno in
(select jobno from emp
inner join career using(empno)
inner join job using(jobno) where empname = 'RICHARD MARTIN')
and deptno in
(select deptno from emp
inner join career using(empno)
inner join dept using(deptno)
where empname = 'RICHARD MARTIN') group by empname;
```

- ОПЕРАТОРЫ ANY/ALL

	- Задание 3.2.4. Найти сведения о номерах сотрудников, получивших за какой-либо месяц зарплату большую, чем средняя зарплата за 2007 г. или большую чем средняя зарплата за 2008г.

```
select empno, salvalue from emp
inner join salary using(empno)
where salvalue > any (select avg(salvalue) from salary
where year = 2007 or year = 2008 group by year);
```

- ОПЕРАТОРЫ ANY/ALL
	- Задание 3.2.5. Найти сведения о номерах сотрудников, получивших зарплату за какой-либо месяц большую, чем средние зарплаты за все годы начислений.

```
select empno, salvalue from emp
inner join salary using(empno)
where salvalue > all (select avg(salvalue) from salary group by year);
```

- ИСПОЛЬЗОВАНИЕ HAVING С ВЛОЖЕННЫМИ ПОДЗАПРОСАМИ

	- Задание 3.2.6. Определить годы, в которые начисленная средняя зарплата была больше средней зарплаты за все годы начислений.
```
select year from salary
group by year having avg(salvalue) > (select avg(salvalue) from salary);
```

- КОРРЕЛИРУЮЩИЕ ПОДЗАПРОСЫ

	- Задание 3.2.7. Определить номера отделов, в которых работали или работают сотрудники, имеющие начисления зарплаты. 
```
select deptname from emp inner join career using(empno)
inner join dept using(deptno)
where empno in (select empno from salary);
```

- ОПЕРАТОР EXISTS

	- Задание 3.2.8. Определить номера отделов, в которых работали или работают сотрудники, имеющие начисления зарплаты.
```
select deptname from career inner join dept using(deptno) where exists
(select empno from salary) group by deptname;
```

- ОПЕРАТОР NOT EXISTS

	- Задание 3.2.9. Определить номера отделов, для сотрудников которых не начислялась зарплата.
```
select * from career where not exists
(select deptno from dept inner join career using(deptno)
inner join emp using(empno) inner join salary using(empno));
```

- СОСТАВНЫЕ ЗАПРОСЫ

	- Задание 3.2.10. Вывести сведения о карьере сотрудников с указанием названий и адресов отделов вместо номеров отделов.
```
select empname, deptname, deptaddr
from emp inner join (career inner join dept using(deptno)) as dept_career
on dept_career.empno = emp.empno;
```

- ОПЕРАТОР CAST

	- Задание 3.2.11. Определить целую часть средних зарплат, по годам начисления.
```
select cast(avg(salvalue) as integer) from salary group by year;
```

- ОПЕРАТОР CASE

	- Задание 3.2.12. Разделите сотрудников на возрастные группы: A) возраст 20-30 лет; B) 31-40 лет; C) 41-50; D) 51-60 или возраст не определён.
```
select empname, case
when date_part('year', age(birthdate)) between 20 and 30
then '20-30'
when date_part('year', age(birthdate)) between 31 and 40
then '31-40'
when date_part('year', age(birthdate)) between 51 and 60
then '51-60'
when birthdate is null then 'unknown'
else 'other' end from emp;
```

- ОПЕРАТОР CASE
	- Задание 3.2.13. Перекодируйте номера отделов, добавив перед номером отдела буквы BI для номеров <=20, буквы LN для номеров >=30.

```
select case when deptno <= 20 then 'BI' || deptname when deptno >= 30 then 'LN' || deptname else deptname end from dept;
```

- ОПЕРАТОР COALESCE (объединяться)
	- Задание 3.2.14. Выдать информацию о сотрудниках из таблицы EMP, заменив отсутствие данного о дате рождения датой '01-01-1000'.
```
select coalesce(birthdate, to_date('01-01-1000', 'DD-MM-YYYY')) from emp;
```

# Задание 3.3

- Рефлексивное соединение. Представление отношений родитель-потомок.
	- Задание 3.3.1. Требуется представить имя каждого сотрудника таблицы EMP а также имя его руководителя.

```
select format('%s works for %s', subordinate.empname, manager.empname)
from emp as subordinate join emp as manager
on manager.empno = subordinate.manager_id;
```

- Иерархический запрос
	- Задание 3.3.2. Требуется представить имя каждого сотрудника таблицы EMP (даже сотрудника, которому не назначен руководитель) и имя его руководителя.

```
with hierarchy(empname, empno, manager_id, manager_name) as (
	select empname, empno, manager_id, '' from emp where manager_id is NULL
	union all
	select s.empname, s.empno, s.manager_id, m.empname from emp as s join emp as m on m.empno = s.manager_id
) select format('%s reports to %s', empname, manager_name) as hierarchy from hierarchy;
```

- Представление отношений потомок-родитель-прародитель
	- Задание 3.3.3. Требуется показать иерархию от CLARK до JOHN KLINTON.

```
with recursive hierarchy(empname, empno, manager_id) as (
	select empname, empno, manager_id from emp where empname='CLARK'
	union
	select m.empname, m.empno, m.manager_id from hierarchy s, emp m
	where s.manager_id = m.empno
) select string_agg(empname, '->') as hierarchy from hierarchy;
```

- Иерархическое представление таблицы
	- Задание 3.3.4. Требуется получить результирующее множество, описывающее иерархию всей таблицы.

```
with recursive hierarchy(hstring, empname, empno, manager_id) as (
	select 'JOHN KLINTON', empname, empno, manager_id from emp where empname='JOHN KLINTON'
	union
	select m.hstring || '->' || s.empname, s.empname, s.empno, s.manager_id from hierarchy m, emp s
	where s.manager_id = m.empno
) select hstring as hierarchy from hierarchy;
```

- Представление уровня иерархии
	- Задание 3.3.5. Требуется показать уровень иерархии каждого сотрудника.

```
with recursive hierarchy(hstring, l, empname, empno, manager_id, path) as (
	select 'JOHN KLINTON', 1, s.empname, s.empno, s.manager_id, cast (array[s.empno] as numeric[])
	from emp s where s.empname='JOHN KLINTON'
	union
	select lpad(s.empname, length(s.empname)+l*4, '    '), m.l+1, s.empname, s.empno, s.manager_id, m.path || s.empno
	from hierarchy m, emp s
	where s.manager_id = m.empno
) select hstring as hierarchy, path from hierarchy order by path;
```

- Выбор всех дочерних строк для заданной строки
	- Задание 3.3.6. Требуется найти всех служащих, которые явно или неявно подчиняются ALLEN.

```
with recursive hierarchy(empname, empno, manager_id) as (
	select empname, empno, manager_id from emp where empname='ALLEN'
	union
	select s.empname, s.empno, s.manager_id from hierarchy m, emp s
	where s.manager_id = m.empno
) select empname as hierarchy from hierarchy;
```

# Задание 3.4

- Добавление, вычитание дней, месяцев, лет
	- Требуется используя значения столбца START_DATE получить дату за десять дней до и после приема на работу, пол года до и после приема на работу, год до и после приема на работу сотрудника JOHN KLINTON.

```
select startdate +
cast('10 days' as interval) as before,
startdate - cast('10 days' as interval) as after
from emp inner join career using(empno) where empname='JOHN KLINTON';
```

- Определение количества дней между двумя датами.
	- Задание 3.4.2. Требуется найти разность между двумя датами и представить результат в днях.  Вычислите разницу в днях между датами приема на работу сотрудников JOHN MARTIN и ALEX BOUSH.
		
```
select 
abs((select startdate from emp inner join career using(empno) where empname = 'JOHN MARTIN') - 
(select startdate from emp inner join career using(empno) where empname = 'ALEX BOUSH')) as diff;
```

- Определение количества месяцев или лет между датами
	- Задание 3.4.3. Требуется найти разность между двумя датами в месяцах и в годах.
```
select to_char(
	justify_interval(make_interval(0, 0, 0, make_date(2004, 02, 04) - make_date(2000, 01, 01))),
	'MM "months" DD "days"');
```

- Определение интервала времени между текущей и следующей записями
	- Задание 3.4.4. Требуется определить интервал времени в днях между двумя датами. Для каждого сотрудника 20-го отделе найти сколько дней прошло между датой его приема на работу и датой приема на работу следующего сотрудника.
```
select empname as emp1, lead(empname, 1) over (order by startdate) as emp2,
make_interval(0, 0, 0, 
	lead(startdate, 1) over (order by startdate)
	- startdate) as diff
from career inner join emp using(empno) where deptno='20';
```

- Определение количества дней в году
	- Задание 3.4.5. Требуется подсчитать количество дней в году по столбцу START_DATE.
```
select (date_trunc('year', startdate)
	+ cast ('1 YEAR' as interval))
	- date_trunc('year', startdate) from career;
```

- Извлечение единиц времени из даты
	- Задание 3.4.6. Требуется разложить текущую дату на день, месяц, год, секунды, минуты, часы.
```
select to_char(now(), 'YYYY-MM-DD hh24:mi:ss');
```

- Определение первого и последнего дней месяца
	- Задание 3.4.7. Требуется получить первый и последний дни текущего месяца.
```
select date_trunc('month', current_date), date_trunc('month', current_date) + interval '1 month' - interval '1 day';
```

- Выбор всех дат года, выпадающих на определенный день недели
	- Задание 3.4.8 Требуется возвратить даты начала и конца каждого из четырех кварталов данного года.
```
select date_trunc('year', current_date) as winter,
	date_trunc('year', current_date) + interval '2 months' as spring,
	date_trunc('year', current_date) + interval '5 months' as summer,
	date_trunc('year', current_date) + interval '8 months' as autumn;
```

- Выбор всех дат года, выпадающих на определенный день недели
	- Задание 3.4.9. Требуется найти все даты года, соответствующие заданному дню недели.

```
with recursive year(day) as (
	values(1)
	union all
	select day + 1 from year 
	where day < abs(extract('days' from date_trunc('year', current_date) - date_trunc('year', current_date + interval '1 year')))
) select date_trunc('year', current_date) + make_interval(0,0,0,day) as monday from year
where extract('dow' from date_trunc('year', current_date) + make_interval(0,0,0,day)) = 1 limit 10;
```

- Создание календаря
	- Задание 3.4.10. Требуется создать календарь на текущий месяц. Календарь должен иметь семь столбцов в ширину и пять строк вниз.

```
drop function get_dow(int);
create function get_dow(day integer) returns integer as $$
	select extract('dow' from make_date(cast (extract('year' from current_date) as int),
	cast (extract('month' from current_date) as int), day)) 
$$ language sql;

with recursive month(day) as (
	values(1)
	union all
	select day + 1 from month 
	where day < date_part('days', date_trunc('month', current_date) 
		+ cast ('1 MONTH' as interval) - cast ('1 DAY' as interval))
) select trunc(day / 7) as week,
	max(case when get_dow(day) = 0 then day end) as sunday,
	max(case when get_dow(day) = 1 then day end) as monday,
	max(case when get_dow(day) = 2 then day end) as tuesday,
	max(case when get_dow(day) = 3 then day end) as wednesday,
	max(case when get_dow(day) = 4 then day end) as thursday,
	max(case when get_dow(day) = 5 then day end) as friday,
	max(case when get_dow(day) = 6 then day end) as saturday
from month group by week order by week;
```

# Самостоятельная часть: таблица

*Вариант 6. Книжный каталог.*

*Описание предметной области.* Домашняя библиотека состоит из книг различной тематики, условно распределенных по различным тематическим каталогам. Книги могут быть написаны как одним автором, так и несколькими авторами (в соавторстве). Могут встречаться разные экземпляры одних и тех же книг одного и того же автора, изданные различными издательствами в разные годы. Книги могут передаваться во временное пользование частным лицам на определенный срок.

*Задачи.* Учитывать книги в домашнем каталоге по тематике, жанру, году издания. Предусмотреть поиск книги по названию, автору, издательству, ключевым словам (или по части слова). Контролировать своевременность возврата книг, переданных во временное пользование.


```
drop view if exists view_book_title;
drop view if exists view_book_author;
drop view if exists view_book_publisher;
drop view if exists view_book_keyword;
drop view if exists view_book_keyword_part;
drop view if exists view_book_count;
drop view if exists view_books_due;
drop table if exists loan;
drop table if exists person;
drop table if exists authorship;
drop table if exists author;
drop table if exists book;
drop table if exists publisher;
drop table if exists genre;
drop table if exists catalogue;

create table catalogue (
	name           varchar(255),
	catalogue_id   integer not null primary key
);

create table genre (
	name           varchar(255),
	genre_id       integer not null primary key,
	catalogue_id   integer not null references catalogue (catalogue_id)
);

create table publisher (
	name           varchar(255),
	country        varchar(255),
	publisher_id   integer not null primary key
);

create table book (
	isbn           integer not null primary key,
	title          varchar(255),
	year_published integer,
	language       varchar(255),
	keywords       varchar(1024),
	genre_id       integer not null references genre (genre_id),
	publisher_id   integer not null references publisher (publisher_id)
);

create table author (
	name           varchar(255),
	surname        varchar(255),
	date_birth     date,
	date_death     date,
	author_id      integer not null primary key
);

create table authorship (
	isbn           integer not null references book (isbn),
	author_id      integer not null references author (author_id),

	primary key (isbn, author_id)
);

create table person (
	name           varchar(255),
	surname        varchar(255),
	phone_number   varchar(255),
	person_id      integer not null primary key
);

create table loan (
	isbn           integer not null references book (isbn),
	person_id      integer not null references person (person_id),
	date_loaned    date,
	date_due       date,
	date_returned  date,

	primary key (isbn, person_id)
);

insert into catalogue (catalogue_id, name) values (10, 'Miscellaneous Literature');
insert into genre (genre_id, catalogue_id, name) values (10, 10, 'Dummy Books');
insert into publisher (publisher_id, name, country) values (10, 'Test Publisher', 'USA');
insert into book 
(isbn, title, year_published, language, keywords, genre_id, publisher_id) 
values (10, 'Test Book', 2020, 'English', 'Test', 10, 10);
insert into author
(author_id, name, surname, date_birth, date_death)
values (10, 'Test', 'Testovich', date '10-02-1952', date '10-10-2020');
insert into authorship (isbn, author_id)
values (10, 10);
insert into person
(person_id, name, surname, phone_number)
values (10, 'John', 'Smith', '+1111111111');
insert into loan
(isbn, person_id, date_loaned, date_due, date_returned)
values (10, 10, date '12-01-2021', date '12-31-2021', date '12-10-2021');
insert into catalogue (catalogue_id, name) values (20, 'Other Catalogue');
insert into genre (genre_id, catalogue_id, name) values (20, 20, 'Other Books');
```

# Задание 3.5 (TODO)

- Задание 3.5.2. Простой CASE

```
select case name
when 'Dummy Books' then 'Тестовые Книги'
when 'Other Books' then 'Другие Книги'
else name end from genre;
```

- Задание 3.5.3. Поисковой CASE

```
select isbn, (case
when extract('day' from date_loaned) < 15 then 'First half of '
else 'Second half of ' end || to_char(date_loaned, 'MonthYYYY')) as date_loaned
from loan;
```

- Задание 3.5.4. WITH

```
with person_average_hold_time as (
select person_id, avg(date_returned - date_loaned) from person inner join loan using(person_id) 
group by person_id) select * from person inner join person_average_hold_time using(person_id);
```

- Задание 3.5.8. NULLIF

```
select isbn, coalesce(nullif(title, 'Test Book'), 'Тестовая Книга') as title from book;
```

- Задание 3.5.11. ROLLUP

```
select count(isbn), coalesce(language, 'Total') from book
group by rollup(language);
```

# Задание 4.1

- Задание 4.1.1. Поднимите нижнюю границу минимальной заработной платы в таблице JOB до 1000$.

```
- Записи в таблице job, соответствующие условию:
select * from job where minsalary < 1000;
--
update job set minsalary = 1000 where minsalary < 1000;
--
```

- Задание 4.1.2. Поднимите минимальную зарплату в таблице JOB на 10% для всех специальностей, кроме финансового директора.

```
\echo Записи в таблице job, соответствующие условию:
select * from job where jobname != 'FINANCIAL DIRECTOR';
--
update job set minsalary = minsalary * 1.1 where jobname != 'FINANCIAL DIRECTOR';
--
```

- Задание 4.1.3. Поднимите минимальную зарплату в таблице JOB на 10% для клерков и на 20% для финансового директора (одним оператором).

```
\echo До
select * from job;
--
update job set minsalary = case when jobname = 'FINANCIAL DIRECTOR' then minsalary * 1.2 else minsalary * 1.1 end;
--
\echo После
select * from job;
```

- Задание 4.1.4. Установите минимальную зарплату финансового директора равной 90% от зарплаты исполнительного директора.

```
\echo До
select * from job where jobname = 'FINANCIAL DIRECTOR' or jobname = 'EXECUTIVE DIRECTOR';
-- 
update job set
minsalary = 0.9*(select minsalary from job where jobname = 'EXECUTIVE DIRECTOR')
where jobname = 'FINANCIAL DIRECTOR';
--
\echo После
select * from job where jobname = 'FINANCIAL DIRECTOR' or jobname = 'EXECUTIVE DIRECTOR';
```

- Задание 4.1.5. Приведите в таблице EMP имена служащих, начинающиеся на букву ‘J’, к нижнему регистру.

```
\echo До
select * from emp where empname like 'J%';
--
update emp set empname = lower(empname) where empname like 'J%';
--
\echo После
select * from emp where empname like 'j%';
```

- Задание 4.1.6. Измените в таблице EMP имена служащих, состоящие из двух слов, так, чтобы оба слова в имени начинались с заглавной буквы, а продолжались прописными.

```
\echo До
select * from emp where empname like '% %';
--
update emp set empname = initcap(empname) where empname like '% %';
--
\echo После
select * from emp where empname like '% %';
```

- Задание 4.1.7. Приведите в таблице EMP имена служащих к верхнему регистру.

```
\echo До
select * from emp where empname like '% %';
--
update emp set empname = upper(empname);
--
\echo После
select * from emp where empname like '% %';
```

- Задание 4.1.8. Перенесите отдел исследований (RESEARCH) в тот же город, в котором расположен отдел продаж (SALES).

```
\echo До
select * from dept where deptname = 'RESEARCH' or deptname = 'SALES';
--
update dept set deptaddr = (select deptaddr from dept where deptname = 'SALES') where deptname = 'RESEARCH';
--
\echo После
select * from dept where deptname = 'RESEARCH' or deptname = 'SALES';
```

\echo Задание 4.1.9. Добавьте нового сотрудника в таблицу EMP. Его имя и фамилия должны совпадать с Вашими, записанными латинскими буквами согласно паспорту, дата рождения также совпадает с Вашей.

```
delete from emp where empno = 8000;
--
insert into emp (empno, empname, birthdate) values (8000, 'Herman Yanush', '2010-10-10');
--
\echo Добавлен новый сотрудник
select * from emp where empno = 8000;
```

- Задание 4.1.10. Определите нового сотрудника (см. предыдущее задание) на работу в бухгалтерию (отдел ACCOUNTING) начиная с текущей даты.

```
delete from career where empno = 8000;
--
insert into career (jobno, empno, deptno, startdate)
values ((select jobno from job where jobname = 'CLERK'), 8000,
(select deptno from dept where deptname = 'ACCOUNTING'), current_date);
--
\echo Сотрудник устроен на работу
select empno, empname, jobno, jobname from emp inner join career using(empno) inner join job using(jobno) where empno = 8000;
```

- Задание 4.1.11. Удалите все записи из таблицы TMP_EMP. Добавьте в нее информацию о сотрудниках, которые работают клерками в настоящий момент.

```
drop table if exists tmp_emp;
create table tmp_emp as table emp;
\echo Сотрудники, соответствующие условию
select * from emp
inner join career using(empno)
inner join job using(jobno)
where jobname = 'CLERK';
--
delete from tmp_emp;
insert into tmp_emp
(empno, empname, birthdate, manager_id)
select empno, empname, birthdate, manager_id from emp
inner join career using(empno)
inner join job using(jobno)
where jobname = 'CLERK';
--
\echo Новая таблица
select * from tmp_emp;
```

- Задание 4.1.12. Добавьте в таблицу TMP_EMP информацию о тех сотрудниках, которые уже не работают на предприятии, а в период работы занимали только одну должность.

```
\echo Сотрудники, соответствующие условию:
select * from emp where empno in
(select empno from career
group by empno having count(startdate) = 1
and max(startdate) < max(enddate));
--
insert into tmp_emp (empno, empname, birthdate, manager_id)
select * from emp where empno in
(select empno from career
group by empno having count(startdate) = 1
and max(startdate) < max(enddate));
--
```

- Задание 4.1.13. Выполните тот же запрос для тех сотрудников, которые никогда не приступали к работе на предприятии.

```
\echo Сотрудники, соответствующие условию:
select * from emp where empno in (select empno from career group by empno having count(startdate) = 1 and max(startdate) is null);
--
insert into tmp_emp (empno, empname, birthdate, manager_id)
select * from emp where empno in 
(select empno from career group by empno having count(startdate) = 1 and max(startdate) is null);
--
```

- Задание 4.1.14. Удалите все записи из таблицы TMP_JOB и добавьте в нее информацию по тем специальностям, которые не используются в настоящий момент на предприятии.

```
drop table if exists tmp_job;
create table tmp_job as table job;
\echo Специальности, соответсвующие условию:
select * from job where jobno in (select jobno from job inner join career using(jobno) group by jobno having count(empno) = 0);
--
delete from tmp_job;
insert into tmp_job (jobno, jobname, minsalary)
select * from job where jobno in
(select jobno from job inner join career using(jobno) group by jobno having count(empno) = 0);
--
```

- Задание 4.1.15. Начислите зарплату в размере 120% минимального должностного оклада всем сотрудникам, работающим на предприятии. Зарплату начислять по должности, занимаемой сотрудником в настоящий момент и отнести ее на прошлый месяц относительно текущей даты.

```
with salary_date as (
	select extract('month' from current_date - interval '1 month') as month, extract('year' from current_date) as year
),
emp_minsalaries as (
	select empno, minsalary from emp inner join career using(empno) inner join job using(jobno)
)
insert into salary
select empno, month, year, minsalary * 1.2 from emp_minsalaries, salary_date as new_salaries; 
```

- Задание 4.1.16. Удалите данные о зарплате за прошлый год.

```
delete from salary where year = extract('year' from current_date - interval '1 year');
```

- Задание 4.1.17. Удалите информацию о карьере сотрудников, которые в настоящий момент уже не работают на предприятии, но когда-то работали.

```
drop table if exists deleted_career;
create table deleted_career as table career;
delete from deleted_career;
with deleted_entries as 
(
	delete from career where empno in (select empno from career group by empno having max(startdate) < max(enddate)) returning *
) insert into deleted_career (jobno, empno, deptno, startdate, enddate) select * from deleted_entries;
```

- Задание 4.1.18. Удалите информацию о начисленной зарплате сотрудников, которые в настоящий момент уже не работают на предприятии (можно использовать результаты работы предыдущего запроса)

```
delete from salary where empno in (select empno from deleted_career);
```

- Задание 4.1.19. Удалите записи из таблицы EMP для тех сотрудников, которые никогда не приступали к работе на предприятии.

```
--
drop table if exists delete_emp;
create table delete_emp as table emp;
delete from delete_emp;

insert into delete_emp (empno, empname, birthdate, manager_id)
select * from emp where empno in
(select empno from emp inner join career using(empno) group by empno); -- удаляем всех 

--(select empno from emp inner join career using(empno)
--group by empno having max(startdate) is null); -- таких не может быть из-за constraint

\echo Список служащих для удаления
select * from delete_emp;

delete from career where empno in (select empno from delete_emp);
delete from salary where empno in (select empno from delete_emp);
-- удаляем записи из таблицы emp в правильном порядке (от менеджеров к подчинённым), чтобы не нарушить constraint
with hierarchy(empname, empno, birthdate, manager_id) as (
	select empname, empno, birthdate, manager_id from emp where manager_id is NULL
	union all
	select s.empname, s.empno, s.birthdate, s.manager_id from emp as s join emp as m on m.empno = s.manager_id
)
delete from emp where empno in (select empno from hierarchy);
--

\echo таблица emp:
select * from emp;
\echo таблица career:
select * from career;
\echo таблица salary:
select * from salary;
```

# Задание 5.1


- Задание 5.1.1. Создайте представление, содержащее данные о сотрудниках пенсионного возраста.

```
drop view if exists retirement_age_emps;
create view retirement_age_emps as select * from emp where age(birthdate) > interval '65 years';
```

- Задание 5.1.2. Создайте представление, содержащее данные об уволенных сотрудниках: имя сотрудника, дата увольнения, отдел, должность.

```
drop view if exists previous_emps;
create view previous_emps as
select * from emp where empno in
(select empno from career group by empno
having max(startdate) < max(enddate));
```

- Задание 5.1.3. Создайте представление, содержащее имя сотрудника, должность, занимаемую сотрудником в данный момент, суммарную заработную плату сотрудника за третий квартал 2010 года. Первый столбец назвать Sotrudnik, второй – Dolzhnost, третий – Itogo_3_kv.

```
drop view if not exists quarter3_salary;
-- Сотрудник может работать на двух работах одновременно, поэтому записи повторяются
create view quarter3_salary as with emp_avg_salary as (
	select empno, avg(salvalue) as total from salary
	where year = 2010 and month between 7 and 9 group by empno
) select startdate, enddate,
empname as Сотрудник, jobname as Должность, total as Итого_3_Квартал
from emp_avg_salary inner join emp using (empno)
inner join career using(empno) inner join job using(jobno)
where date '2010-07-01' > startdate
and (enddate is null or enddate > '2010-10-30');
```

- Задание 5.1.4. На основе представления из задания 2 и таблицы SALARY создайте представление, содержащее данные об уволенных сотрудниках, которым зарплата начислялась более 2 раз. В созданном представлении месяц начисления зарплаты и сумма зарплаты вывести в одном столбце, в качестве разделителя использовать запятую.

```
select empname, year || '.' || lpad(cast (month as text), 2, '0') || ', ' || salvalue as month_salary
from previous_emps inner join salary using(empno)
where empno in
(select empno from emp
inner join salary using(empno) group by empno having count(*) > 2)
order by year, month;
```

# Задание 5.2

```
drop view if exists view_book_title;
drop view if exists view_book_author;
drop view if exists view_book_publisher;
drop view if exists view_book_keyword;
drop view if exists view_book_keyword_part;
drop view if exists view_book_count;
drop view if exists view_books_due;

-- Поиск книг по названию
create view view_book_title as select * from book where title = 'Test Book';
-- Поиск книг по автору
create view view_book_author as
select * from book inner join authorship using(isbn)
inner join author using(author_id)
where author.name = 'Test'
and author.surname = 'Testovich';
-- Поиск книг по издательсвту
create view view_book_publisher as
select * from book inner join publisher using(publisher_id)
where publisher.name = 'Test Publisher';
-- Поиск книг по ключевому слову (через массивы)
create view view_book_keyword as select * from book where string_to_array(keywords, ' ') @> ARRAY['Test'];
-- Поиск книг по части ключевого слова (через like)
create view view_book_keyword_part as select * from book where keywords like '%Tes%';
-- Количество книг в каждом катологе
create view view_book_count as
select catalogue_id, count(isbn) from catalogue
inner join genre using(catalogue_id) inner join book using(genre_id)
-- Книги, подлежащие возврату в ближайшие 30 дней
group by catalogue_id;
create view view_books_due as
select title, date_due from loan inner join book using(isbn)
where date_due - current_date < 30;
```

# Задание 6.2 (TODO)

```
drop routine if exists change_genre;
drop trigger if exists loan_validate on loan;
drop routine if exists loan_validate;

-- Изменение жанра книги (а значит и католога) по ключевым словам
create function change_genre(keywords varchar, new_genre integer)
returns integer as $$
	with moved_books as (
		update book set genre_id=new_genre
		where genre_id != new_genre
		and string_to_array(book.keywords, ' ') @> string_to_array(keywords, ' ')
		returning *) select count(*) as moved from moved_books;
$$ language sql;

-- Валидация таблицы loan
create function loan_validate() returns trigger as $$
begin
	if new.date_loaned > new.date_due then
		raise exception 'date_loaned cannot be greater than date_due';
	end if;
	if new.date_loaned > new.date_returned then
		raise exception 'date_loaned cannot be greater than date_returned';
	end if;
	return new;
end;
$$ language plpgsql;

create trigger loan_validate before insert or update on loan
for each row execute function loan_validate();
```
