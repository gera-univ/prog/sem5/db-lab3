\echo Задание 5.1.1. Создайте представление, содержащее данные о сотрудниках пенсионного возраста.

drop view if exists retirement_age_emps;
create view retirement_age_emps as select * from emp where age(birthdate) > interval '65 years';

\echo Задание 5.1.2. Создайте представление, содержащее данные об уволенных сотрудниках: имя сотрудника, дата увольнения, отдел, должность.

drop view if exists previous_emps;
create view previous_emps as select * from emp where empno in (select empno from career group by empno having max(startdate) < max(enddate));

\echo Задание 5.1.3. Создайте представление, содержащее имя сотрудника, должность, занимаемую сотрудником в данный момент, суммарную заработную плату сотрудника за третий квартал 2010 года. Первый столбец назвать Sotrudnik, второй – Dolzhnost, третий – Itogo_3_kv.

drop view if not exists quarter3_salary;
-- Сотрудник может работать на двух работах одновременно, поэтому записи повторяются
create view quarter3_salary as with emp_avg_salary as (
	select empno, avg(salvalue) as total from salary where year = 2010 and month between 7 and 9 group by empno
) select startdate, enddate, empname as Сотрудник, jobname as Должность, total as Итого_3_Квартал
from emp_avg_salary inner join emp using (empno) inner join career using(empno) inner join job using(jobno) where date '2010-07-01' > startdate and (enddate is null or enddate > '2010-10-30');

\echo Задание 5.1.4. На основе представления из задания 2 и таблицы SALARY создайте представление, содержащее данные об уволенных сотрудниках, которым зарплата начислялась более 2 раз. В созданном представлении месяц начисления зарплаты и сумма зарплаты вывести в одном столбце, в качестве разделителя использовать запятую.

select empname, year || '.' || lpad(cast (month as text), 2, '0') || ', ' || salvalue as month_salary from previous_emps inner join salary using(empno) where empno in (select empno from emp inner join salary using(empno) group by empno having count(*) > 2) order by year, month;
