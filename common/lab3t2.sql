-- ПОДЗАПРОСЫ, ВЫБИРАЮЩИЕ ОДНУ СТРОКУ

\echo Задание 3.2.1. Найти имена сотрудников, получивших за годы начисления зарплаты минимальную зарплату.
select * from emp inner join salary using(empno) where salvalue = (select min(salvalue) from salary);

-- ПОДЗАПРОСЫ, ВОЗВРАЩАЮЩИЕ БОЛЕЕ ОДНОЙ СТРОКИ

\echo Задание 3.2.2. Найти имена сотрудников, работавших или работающих в тех же отделах, в которых работал или работает сотрудник с именем RICHARD MARTIN.
select empname from emp inner join career using(empno) where deptno in (select deptno from emp inner join career using(empno) inner join dept using(deptno) where empname = 'RICHARD MARTIN') group by empname;

-- СРАВНЕНИЕ БОЛЕЕ ЧЕМ ПО ОДНОМУ ЗНАЧЕНИЮ

\echo Задание 3.2.3. Найти имена сотрудников, работавших или работающих в тех же отделах и должностях, что и сотрудник 'RICHARD MARTIN'.
select empname from career inner join emp using(empno) where jobno in (select jobno from emp inner join career using(empno) inner join job using(jobno) where empname = 'RICHARD MARTIN') and deptno ins(select deptno from emp inner join career using(empno) inner join dept using(deptno) where empname = 'RICHARD MARTIN') group by empname;

-- ОПЕРАТОРЫ ANY/ALL

\echo Задание 3.2.4. Найти сведения о номерах сотрудников, получивших за какой-либо месяц зарплату большую, чем средняя зарплата за 2007 г. или большую чем средняя зарплата за 2008г.
select empno, salvalue from emp inner join salary using(empno) where salvalue > any (select avg(salvalue) from salary where year = 2007 or year = 2008 group by year);

\echo Задание 3.2.5. Найти сведения о номерах сотрудников, получивших зарплату за какой-либо месяц большую, чем средние зарплаты за все годы начислений.
select empno, salvalue from emp inner join salary using(empno) where salvalue > all (select avg(salvalue) from salary group by year);

-- ИСПОЛЬЗОВАНИЕ HAVING С ВЛОЖЕННЫМИ ПОДЗАПРОСАМИ

\echo Задание 3.2.6. Определить годы, в которые начисленная средняя зарплата была больше средней зарплаты за все годы начислений.
select year from salary group by year having avg(salvalue) > (select avg(salvalue) from salary);

-- КОРРЕЛИРУЮЩИЕ ПОДЗАПРОСЫ

\echo Задание 3.2.7. Определить номера отделов, в которых работали или работают сотрудники, имеющие начисления зарплаты. 
select deptname from emp inner join career using(empno) inner join dept using(deptno) where empno in (select empno from salary);

-- ОПЕРАТОР EXISTS

\echo Задание 3.2.8. Определить номера отделов, в которых работали или работают сотрудники, имеющие начисления зарплаты.
select deptname from career inner join dept using(deptno) where exists (select empno from salary) group by deptname;

-- ОПЕРАТОР NOT EXISTS

\echo Задание 3.2.9. Определить номера отделов, для сотрудников которых не начислялась зарплата.
select * from career where not exists (select deptno from dept inner join career using(deptno) inner join emp using(empno) inner join salary using(empno));

-- СОСТАВНЫЕ ЗАПРОСЫ

\echo Задание 3.2.10. Вывести сведения о карьере сотрудников с указанием названий и адресов отделов вместо номеров отделов.
select empname, deptname, deptaddr from emp inner join (career inner join dept using(deptno)) as dept_career on dept_career.empno = emp.empno;

-- ОПЕРАТОР CAST

\echo Задание 3.2.11. Определить целую часть средних зарплат, по годам начисления.
select cast(avg(salvalue) as integer) from salary group by year;

-- ОПЕРАТОР CASE

\echo Задание 3.2.12. Разделите сотрудников на возрастные группы: A) возраст 20-30 лет; B) 31-40 лет; C) 41-50; D) 51-60 или возраст не определён.
select empname, case when date_part('year', age(birthdate)) between 20 and 30 then '20-30' when date_part('year', age(birthdate)) between 31 and 40 then '31-40' when date_part('year', age(birthdate)) between 51 and 60 then '51-60' when birthdate is null then 'unknown' else 'other' end from emp;

\echo Задание 3.2.13. Перекодируйте номера отделов, добавив перед номером отдела буквы BI для номеров <=20, буквы LN для номеров >=30.
select case when deptno <= 20 then 'BI' || deptname when deptno >= 30 then 'LN' || deptname else deptname end from dept;

-- ОПЕРАТОР COALESCE (объединяться)

\echo Задание 3.2.14. Выдать информацию о сотрудниках из таблицы EMP, заменив отсутствие данного о дате рождения датой '01-01-1000'.
select coalesce(birthdate, to_date('01-01-1000', 'DD-MM-YYYY')) from emp;
