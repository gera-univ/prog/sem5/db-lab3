\echo Задание 4.1.1. Поднимите нижнюю границу минимальной заработной платы в таблице JOB до 1000$.

\echo Записи в таблице job, соответствующие условию:
select * from job where minsalary < 1000;
--
update job set minsalary = 1000 where minsalary < 1000;
--

\echo Задание 4.1.2. Поднимите минимальную зарплату в таблице JOB на 10% для всех специальностей, кроме финансового директора.

\echo Записи в таблице job, соответствующие условию:
select * from job where jobname != 'FINANCIAL DIRECTOR';
--
update job set minsalary = minsalary * 1.1 where jobname != 'FINANCIAL DIRECTOR';
--

\echo Задание 4.1.3. Поднимите минимальную зарплату в таблице JOB на 10% для клерков и на 20% для финансового директора (одним оператором).

\echo До
select * from job;
--
update job set minsalary = case when jobname = 'FINANCIAL DIRECTOR' then minsalary * 1.2 else minsalary * 1.1 end;
--
\echo После
select * from job;

\echo Задание 4.1.4. Установите минимальную зарплату финансового директора равной 90% от зарплаты исполнительного директора.

\echo До
select * from job where jobname = 'FINANCIAL DIRECTOR' or jobname = 'EXECUTIVE DIRECTOR';
-- 
update job set minsalary = 0.9*(select minsalary from job where jobname = 'EXECUTIVE DIRECTOR') where jobname = 'FINANCIAL DIRECTOR';
--
\echo После
select * from job where jobname = 'FINANCIAL DIRECTOR' or jobname = 'EXECUTIVE DIRECTOR';

\echo Задание 4.1.5. Приведите в таблице EMP имена служащих, начинающиеся на букву ‘J’, к нижнему регистру.

\echo До
select * from emp where empname like 'J%';
--
update emp set empname = lower(empname) where empname like 'J%';
--
\echo После
select * from emp where empname like 'j%';

\echo Задание 4.1.6. Измените в таблице EMP имена служащих, состоящие из двух слов, так, чтобы оба слова в имени начинались с заглавной буквы, а продолжались прописными.

\echo До
select * from emp where empname like '% %';
--
update emp set empname = initcap(empname) where empname like '% %';
--
\echo После
select * from emp where empname like '% %';

\echo Задание 4.1.7. Приведите в таблице EMP имена служащих к верхнему регистру.

\echo До
select * from emp where empname like '% %';
--
update emp set empname = upper(empname);
--
\echo После
select * from emp where empname like '% %';

\echo Задание 4.1.8. Перенесите отдел исследований (RESEARCH) в тот же город, в котором расположен отдел продаж (SALES).

\echo До
select * from dept where deptname = 'RESEARCH' or deptname = 'SALES';
--
update dept set deptaddr = (select deptaddr from dept where deptname = 'SALES') where deptname = 'RESEARCH';
--
\echo После
select * from dept where deptname = 'RESEARCH' or deptname = 'SALES';

\echo Задание 4.1.9. Добавьте нового сотрудника в таблицу EMP. Его имя и фамилия должны совпадать с Вашими, записанными латинскими буквами согласно паспорту, дата рождения также совпадает с Вашей.

delete from emp where empno = 8000;
--
insert into emp (empno, empname, birthdate) values (8000, 'Herman Yanush', '2010-10-10');
--
\echo Добавлен новый сотрудник
select * from emp where empno = 8000;

\echo Задание 4.1.10. Определите нового сотрудника (см. предыдущее задание) на работу в бухгалтерию (отдел ACCOUNTING) начиная с текущей даты.

delete from career where empno = 8000;
--
insert into career (jobno, empno, deptno, startdate) values ((select jobno from job where jobname = 'CLERK'), 8000, (select deptno from dept where deptname = 'ACCOUNTING'), current_date);
--
\echo Сотрудник устроен на работу
select empno, empname, jobno, jobname from emp inner join career using(empno) inner join job using(jobno) where empno = 8000;

\echo Задание 4.1.11. Удалите все записи из таблицы TMP_EMP. Добавьте в нее информацию о сотрудниках, которые работают клерками в настоящий момент.

drop table if exists tmp_emp;
create table tmp_emp as table emp;
\echo Сотрудники, соответствующие условию
select * from emp inner join career using(empno) inner join job using(jobno) where jobname = 'CLERK';
--
delete from tmp_emp;
insert into tmp_emp (empno, empname, birthdate, manager_id) select empno, empname, birthdate, manager_id from emp inner join career using(empno) inner join job using(jobno) where jobname = 'CLERK';
--
\echo Новая таблица
select * from tmp_emp;

\echo Задание 4.1.12. Добавьте в таблицу TMP_EMP информацию о тех сотрудниках, которые уже не работают на предприятии, а в период работы занимали только одну должность.

\echo Сотрудники, соответствующие условию:
select * from emp where empno in (select empno from career group by empno having count(startdate) = 1 and max(startdate) < max(enddate));
--
insert into tmp_emp (empno, empname, birthdate, manager_id)
select * from emp where empno in
(select empno from career group by empno having count(startdate) = 1 and max(startdate) < max(enddate));
--

\echo Задание 4.1.13. Выполните тот же запрос для тех сотрудников, которые никогда не приступали к работе на предприятии.

\echo Сотрудники, соответствующие условию:
select * from emp where empno in (select empno from career group by empno having count(startdate) = 1 and max(startdate) is null);
--
insert into tmp_emp (empno, empname, birthdate, manager_id)
select * from emp where empno in 
(select empno from career group by empno having count(startdate) = 1 and max(startdate) is null);
--

\echo Задание 4.1.14. Удалите все записи из таблицы TMP_JOB и добавьте в нее информацию по тем специальностям, которые не используются в настоящий момент на предприятии.

drop table if exists tmp_job;
create table tmp_job as table job;
\echo Специальности, соответсвующие условию:
select * from job where jobno in (select jobno from job inner join career using(jobno) group by jobno having count(empno) = 0);
--
delete from tmp_job;
insert into tmp_job (jobno, jobname, minsalary)
select * from job where jobno in
(select jobno from job inner join career using(jobno) group by jobno having count(empno) = 0);
--

\echo Задание 4.1.15. Начислите зарплату в размере 120% минимального должностного оклада всем сотрудникам, работающим на предприятии. Зарплату начислять по должности, занимаемой сотрудником в настоящий момент и отнести ее на прошлый месяц относительно текущей даты.

with salary_date as (
	select extract('month' from current_date - interval '1 month') as month, extract('year' from current_date) as year
),
emp_minsalaries as (
	select empno, minsalary from emp inner join career using(empno) inner join job using(jobno)
)
insert into salary
select empno, month, year, minsalary * 1.2 from emp_minsalaries, salary_date as new_salaries; 

\echo Задание 4.1.16. Удалите данные о зарплате за прошлый год.

delete from salary where year = extract('year' from current_date - interval '1 year');

\echo Задание 4.1.17. Удалите информацию о карьере сотрудников, которые в настоящий момент уже не работают на предприятии, но когда-то работали.

drop table if exists deleted_career;
create table deleted_career as table career;
delete from deleted_career;
with deleted_entries as 
(
	delete from career where empno in (select empno from career group by empno having max(startdate) < max(enddate)) returning *
) insert into deleted_career (jobno, empno, deptno, startdate, enddate) select * from deleted_entries;

\echo Задание 4.1.18. Удалите информацию о начисленной зарплате сотрудников, которые в настоящий момент уже не работают на предприятии (можно использовать результаты работы предыдущего запроса)

delete from salary where empno in (select empno from deleted_career);

\echo Задание 4.1.19. Удалите записи из таблицы EMP для тех сотрудников, которые никогда не приступали к работе на предприятии.

--
drop table if exists delete_emp;
create table delete_emp as table emp;
delete from delete_emp;

insert into delete_emp (empno, empname, birthdate, manager_id)
select * from emp where empno in
(select empno from emp inner join career using(empno) group by empno); -- удаляем всех 
--(select empno from emp inner join career using(empno) group by empno having max(startdate) is null); -- таких не может быть из-за constraint

\echo Список служащих для удаления
select * from delete_emp;

delete from career where empno in (select empno from delete_emp);
delete from salary where empno in (select empno from delete_emp);
-- удаляем записи из таблицы emp в правильном порядке (от менеджеров к подчинённым), чтобы не нарушить constraint
with hierarchy(empname, empno, birthdate, manager_id) as (
	select empname, empno, birthdate, manager_id from emp where manager_id is NULL
	union all
	select s.empname, s.empno, s.birthdate, s.manager_id from emp as s join emp as m on m.empno = s.manager_id
)
delete from emp where empno in (select empno from hierarchy);
--

\echo таблица emp:
select * from emp;
\echo таблица career:
select * from career;
\echo таблица salary:
select * from salary;
