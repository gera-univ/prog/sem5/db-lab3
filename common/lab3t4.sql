-- Добавление, вычитание дней, месяцев, лет
\echo Требуется используя значения столбца START_DATE получить дату за десять дней до и после приема на работу, пол года до и после приема на работу, год до и после приема на работу сотрудника JOHN KLINTON.
select startdate + cast('10 days' as interval) as before, startdate - cast('10 days' as interval) as after from emp inner join career using(empno) where empname='JOHN KLINTON';

-- Определение количества дней между двумя датами.
\echo Задание 3.4.2. Требуется найти разность между двумя датами и представить результат в днях.  Вычислите разницу в днях между датами приема на работу сотрудников JOHN MARTIN и ALEX BOUSH.
select 
abs((select startdate from emp inner join career using(empno) where empname = 'JOHN MARTIN') - 
(select startdate from emp inner join career using(empno) where empname = 'ALEX BOUSH')) as diff;

-- Определение количества месяцев или лет между датами
\echo Задание 3.4.3. Требуется найти разность между двумя датами в месяцах и в годах.
select to_char(
	justify_interval(make_interval(0, 0, 0, make_date(2004, 02, 04) - make_date(2000, 01, 01))),
	'MM "months" DD "days"');

-- Определение интервала времени между текущей и следующей записями
\echo Задание 3.4.4. Требуется определить интервал времени в днях между двумя датами. Для каждого сотрудника 20-го отделе найти сколько дней прошло между датой его приема на работу и датой приема на работу следующего сотрудника.
select empname as emp1, lead(empname, 1) over (order by startdate) as emp2,
make_interval(0, 0, 0, 
	lead(startdate, 1) over (order by startdate)
	- startdate) as diff
from career inner join emp using(empno) where deptno='20';

-- Определение количества дней в году
\echo Задание 3.4.5. Требуется подсчитать количество дней в году по столбцу START_DATE.
select (date_trunc('year', startdate)
	+ cast ('1 YEAR' as interval))
	- date_trunc('year', startdate) from career;

-- Извлечение единиц времени из даты
\echo Задание 3.4.6. Требуется разложить текущую дату на день, месяц, год, секунды, минуты, часы.
select to_char(now(), 'YYYY-MM-DD hh24:mi:ss');

-- Определение первого и последнего дней месяца
\echo Задание 3.4.7. Требуется получить первый и последний дни текущего месяца.
select date_trunc('month', current_date), date_trunc('month', current_date) + interval '1 month' - interval '1 day';

-- Выбор всех дат года, выпадающих на определенный день недели
\echo Задание 3.4.8 Требуется возвратить даты начала и конца каждого из четырех кварталов данного года.
select date_trunc('year', current_date) as winter,
	date_trunc('year', current_date) + interval '2 months' as spring,
	date_trunc('year', current_date) + interval '5 months' as summer,
	date_trunc('year', current_date) + interval '8 months' as autumn;

-- Выбор всех дат года, выпадающих на определенный день недели
\echo Задание 3.4.9. Требуется найти все даты года, соответствующие заданному дню недели.
with recursive year(day) as (
	values(1)
	union all
	select day + 1 from year 
	where day < abs(extract('days' from date_trunc('year', current_date) - date_trunc('year', current_date + interval '1 year')))
) select date_trunc('year', current_date) + make_interval(0,0,0,day) as monday from year
where extract('dow' from date_trunc('year', current_date) + make_interval(0,0,0,day)) = 1 limit 10;

-- Создание календаря
\echo Задание 3.4.10. Требуется создать календарь на текущий месяц. Календарь должен иметь семь столбцов в ширину и пять строк вниз.

drop function get_dow(int);
create function get_dow(day integer) returns integer as $$
	select extract('dow' from make_date(cast (extract('year' from current_date) as int),
	cast (extract('month' from current_date) as int), day)) 
$$ language sql;

with recursive month(day) as (
	values(1)
	union all
	select day + 1 from month 
	where day < date_part('days', date_trunc('month', current_date) 
		+ cast ('1 MONTH' as interval) - cast ('1 DAY' as interval))
) select trunc(day / 7) as week,
	max(case when get_dow(day) = 0 then day end) as sunday,
	max(case when get_dow(day) = 1 then day end) as monday,
	max(case when get_dow(day) = 2 then day end) as tuesday,
	max(case when get_dow(day) = 3 then day end) as wednesday,
	max(case when get_dow(day) = 4 then day end) as thursday,
	max(case when get_dow(day) = 5 then day end) as friday,
	max(case when get_dow(day) = 6 then day end) as saturday
from month group by week order by week;

